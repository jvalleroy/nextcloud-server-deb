#! /bin/make -f

all:
	[ -n $(PHP_VER) ]
	find tree -type f -name '*.in' -exec scripts/process $(PHP_VER) {} \;
	mkdir -p tree/usr/share/man/man8
	pandoc --standalone --to man nextcloud-server.8.md -o tree/usr/share/man/man8/nextcloud-server.8


install:
	find tree -type f ! -name '*.in' -exec scripts/install {} $(DESTDIR)$(PREFIX)  \;

clean:
	find tree -type f -name '*.in' -exec scripts/clean {} \;
	rm -f tree/usr/share/man/man8/nextcloud-server.8
	[ ! -e tree/usr/share/man/man8 ] || rmdir tree/usr/share/man/man8
	[ ! -e tree/usr/share/man ] || rmdir tree/usr/share/man
