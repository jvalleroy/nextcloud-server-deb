Debian integration of Nextcloud Server.

# Official Resources
  * [Nextcloud Website](https://nextcloud.com)
  * [Nextcloud Git Repository](https://github.com/nextcloud)

# Highlights
  * Runs as unprivileged user, with an apparmor profile.
  * Compatible with in-place snapshotting (experimental).
  * Well-integrated: you aren't installing a second set of dependencies.
  * We download Nextcloud Server from the official site, check its
    signature, and then install it.  Packaging and upstream are __decoupled__.
  * Small footprint: most content is kept in a squashfs.
  * Authentication to mysql is via unix socket.
  * Run `nextcloud-server upgrade` to perform an upgrade: it downloads the
    latest version, and runs all the steps.
  * In-place snapshot, compatible with filesystem snapshotting.  **VERY EXPERIMENTAL**!

# To-do list:
  * Redis

# Important!
This requires apparmor! See the [Debian AppArmor Howto](https://wiki.debian.org/AppArmor/HowToUse).
Be sure to reboot after changing the grub command line.

# Using the repository
Add the following apt-source.list line:

> deb https://www.antonioerusso.com/repositories/debian /

Use my posted gpg key to authenticate, either here or [my site](https://www.antonioerusso.com).
Please don't trust random repositories!  You may need to `apt install apt-transport-https`.

# Installing from source

```bash
git clone https://gitlab.com/aerusso/nextcloud-server-deb.git
cd nextcloud-server-deb
dpkg-buildpackage -b -us -uc
cd ..
sudo apt install ./nextcloud-server_*.deb
```

# Compatibility Notes
 * I have an install that I am maintaining with this, so I am making
   sure upgrades are __possible__.
 * Upgrade failures are not bugs (yet). I'll post notes about
   how to fix things.
 * To wit, 0.1 decouples the packaging and upstream, and renames things
   to avoid conflicts with the nextcloud client.
 * Upgrading to 0.1.4 may require `nextcloud-server start`.

```bash
nextcloud-server upgrade --force 16.0.4
mkdir /var/lib/nextcloud-server/temp
chown nextcloud:nextcloud /var/lib/nextcloud-server/temp
```
  which will download and install the files that were included in the
  package before.  You'll also need to manually adjust
  `/etc/nextcloud/config.php` to reflect
  the new directory structure (under `/var/lib/nextcloud-server` instead
  of `/var/lib/nextcloud`).  Similarly, change the home directory
  of the `nextcloud` user.
