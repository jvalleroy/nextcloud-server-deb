% NEXTCLOUD-SERVER(8) Version 0.1.11 | Administrative Documentation

NAME
====

nextcloud-server - administrative frontend to the Nextcloud server

SYNOPSIS
========

| **nextcloud-server** _COMMAND_ [_SUBOPTION_]

# install

Performs an automatic configuration of nextcloud, including:

- Setting up the mariadb database and user
- Configuring nginx webserver
- Obtaining SSL certificates
- Performing the nextcloud specific initialization

 Can be pre-seeded with debconf values, or alternatively preconfigure.

# purge

Destroys a site, its database, and all files.

# start

Ensures that all required daemons are enabled, started, and with appropriate
configuration loaded (reloading if necessary).  Ensures that the web site
image is mounted.

# stop

Inverse of start, deconfiguring all daemons and shutting down php-fpm if it is
not needed.

# preconfigure

Pre-seed domain and admin password information.

# refresh-webconf

| **refresh-webconf** [_DOMAIN_] [_WEBCONF_]

Construct an nginx webserver configuration file from

 /etc/nextcloud/nginx.template ,

and move it to WEBCONF, or the default

 /etc/nginx/sites-available/nextcloud .

Uses certbot to configure SSL.  Use either debconf configured domain name, or
(untested) DOMAIN.

# occ

| **occ** [_OCC OPTIONS_]

Front-end to the the nextcloud occ utility. Invalidates the config.php cache
afterwards, in case any changes were made.

# php

| **php** [_PHP OPTIONS_]

Starts php in an appropriate environment.

# exec

| **exec** [_COMMAND_]

Run a command as the nextcloud user, with an appropriate environment, confined
at the exact same level as the php-fpm worker threads.

# download

| **download** [**\--keep-orig**] [_VERSION_]

Downloads _VERSION_ (or the latest if _VERSION_ is the empty string). Does not
delete the downloaded tar.bz2 if **--keep-orig** is passed.

# upgrade

| **update** [**--keep-old**] [**--force**] [_VERSION_]

Downloads as above, and then performs an upgrade.  Deletes the old image unless
**--keep-old** is passed.  **--force** skips the version check, and implies
**--keep-old**.

# SNAPSHOTS (EXPERIMENTAL)

| **snapshot-**[**un**]**lock**

Locks or unlocks Nextcloud, preparing for a snapshot. E.g.,:

```bash
# nextcloud-server snapshot-lock
# rsync -a /var/lib/nextcloud-server /backups/nextcloud
# nextcloud-server snapshot-unlock
```

# SNAPSHOT RESTORE (EXPERIMENTAL)

## restore

Assumes the server is shut down, there is no `nextcloud` database, and a
snapshot is already placed in

/var/lib/nextcloud-server

Imports the site.  This command acts standalone.  If you need more fine-grained
control over the restore, consider using restore-prepare/restore-finish.

## restore-prepare

Shuts down nextcloud and the SQL server, putting the system in an appropriate
state to restore a snapshot to

/var/lib/nextcloud-server

Call finish-restore after doing so.

## restore-finish

**/var/lib/nextcloud-server MUST be as during the snapshot.**

Copies /var/lib/nextcloud-server/etc back to

/etc/nextcloud ,

and restores the database, using *sql-schema*, *sql-tables*, and the underlying
InnoDB files (in table per file format).

# UNDOCUMENTED INTERNAL COMMANDS

| **internal-snap** _LOCKFILE_ _RELEASEFILE_

Locks the database after synchronizing

/etc/nextcloud

to

/var/lib/nextcloud-server/etc

and putting nextcloud into maintenance mode. Creates _RELEASEFILE_, and then
waits until _LOCKFILE_ does not exist. Releases the database lock, removes
_RELEASEFILE_, and leaves maintenance mode.

IF this works correctly, backing up

/var/lib/nextcloud-server

is all that is needed for a backup, while the lock is held.

**Use snapshot-[un]lock instead.**

| **refresh-sql**

creates an appropriate nextcloud sql user.

| **refresh-permissions** _etc_

sets up permissions.
